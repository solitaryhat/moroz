﻿using UnityEngine;

public class LockpickControls : MonoBehaviour{
    Vector3 _moveUpDirection;

    Rigidbody2D _rigidBody;

    private int _startX = 24;
    private int _offset = 70;

    private Vector2 _deltaTouch = new Vector2(0, 0);

    void Start()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        Physics2D.IgnoreLayerCollision(0, 10, true);
    }

	void Update ()
    {
        for (int i = 0; i < Input.touchCount; i++)
        {
            _deltaTouch = Input.GetTouch(i).deltaPosition;
            if ((Input.GetTouch(i).phase == TouchPhase.Moved || Input.GetTouch(i).phase == TouchPhase.Began) && Input.touchCount == 1)
            {
                Move();
            }
        }
        if (Input.touchCount == 0)
        {
            _rigidBody.gravityScale = 50;
        }
        else
        {
            _rigidBody.gravityScale = 0;
        }
	}

    public void Move()
    {
        float X = _rigidBody.position.x + _deltaTouch.x;
        float Y = _rigidBody.position.y + _deltaTouch.y;
        _rigidBody.MovePosition(new Vector2(X, Y));
    }

    void GoToStart()
    {
        transform.position = new Vector3(transform.position.x, -85, 1);
    }
}

﻿public static class PropertiesHandler
{
    public static bool IsTutorialGame = false;
    public static long MoneyAll = 0;
    public static int[] MoneyCosts = 
        {
            312, 
            313,
            625, 
            1250,
            2500,
            5000,
            10000,
            20000,
            40000,
            80000,
            160000
        };
    public static float[] Timers =
        {
            155,
            140,
            135,
            120,
            105,
            90,
            75,
            60,
            45,
            30,
            15
        };
    public static int LevelIndex = 0;
}


﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HelpHelper : MonoBehaviour {

    public GameObject _helpCanvas;

	// Use this for initialization
	void Start () {
        _helpCanvas.SetActive(false);
	}

    public void OnHelpTouched()
    {
        _helpCanvas.SetActive(true);
    }

    public void OnHelpCloseTouched()
    {
        _helpCanvas.SetActive(false);
    }
}

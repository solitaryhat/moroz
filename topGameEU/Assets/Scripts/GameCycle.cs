﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class GameCycle : MonoBehaviour {

    public GameObject _lockPick;
    public GameObject _rod0;
    public GameObject _rod1;
    public GameObject _rod2;
    public GameObject _rod3;
    public AudioSource _audioSourceBroken;
    public AudioSource _audioSourceOpen;
    public AudioSource _audioSourceOpenRod;
    public Animator _animator;
    public GameObject _zamok;
    public GameObject _zamokThing;
    public Animator _zamokThingAnimator;

    public GameObject _rph;
    public GameObject _lock;
    public GameObject _lockSpace;

    public Text _timer;
    public Text _money;

    private int _openedRods;
    private float _timeRemain;

    public GameObject _blinkBg;
    public Animator _lockAnim;


	void Start () {
        _timeRemain = PropertiesHandler.Timers[PropertiesHandler.LevelIndex];
        _openedRods = 0;
        _animator.enabled = false;
        _zamokThingAnimator.enabled = false;
        _lockAnim.enabled = false;
        _blinkBg.SetActive(false);


        if (PropertiesHandler.IsTutorialGame)
        {
            _zamok.SetActive(false);
            _zamokThing.SetActive(false);
        }
        _money.text = PropertiesHandler.MoneyAll.ToString() + " / 160000"; ;
    }

    void Update () {
        _timeRemain -= Time.deltaTime;
        if (!PropertiesHandler.IsTutorialGame && (_zamokThingAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !_zamokThingAnimator.IsInTransition(0)))
        {
            SceneManager.LoadScene("SuccessScene");
        }
        else if (_animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !_animator.IsInTransition(0))
        {
            SceneManager.LoadScene("SuccessScene");
        }

    }

    void OnGUI()
    {
        _timer.text = GetTime();
    }

    private void HandleTimer()
    {
        _timeRemain -= Time.deltaTime;
        if (_timeRemain <= 0)
        {
            SceneManager.LoadScene("FailScene");
        }
    }
    
    private string GetTime()
    {
        int timeAsInt = (int)_timeRemain;
        int minutes = timeAsInt / 60;
        int seconds = timeAsInt % 60;
        string time = minutes.ToString() + ":" + (seconds < 10 ? "0" : "") + seconds.ToString();
        if (time == "0:00")
        {
            SceneManager.LoadScene("FailScene");
        }
        return time;
    }

    void OnOpen(int rodId)
    {
        _openedRods++;
        _audioSourceOpenRod.Play();
        if (_openedRods == 4)
        {
            _audioSourceOpen.Play();
            if (PropertiesHandler.IsTutorialGame)
            {
                _animator.enabled = true;
            }
            else
            {
                HideLockElements();
                _zamokThingAnimator.enabled = true;
            }
        }
    }

    void OnBroke(int rodId)
    {
        _audioSourceBroken.Play();
        Debug.Log("OnBroke " + rodId.ToString());
        _lockPick.SendMessage("GoToStart");
        NotifyRefreshRods();
        _openedRods = 0;
    }

    private void HideLockElements()
    {
        _rph.SetActive(false);
        _lock.SetActive(false);
        _rod2.SetActive(false);
        _lockSpace.SetActive(false);
    }

    private void NotifyRefreshRods()
    {
        _rod0.SendMessage("Refresh");
        _rod1.SendMessage("Refresh");
        _rod2.SendMessage("Refresh");
        _rod3.SendMessage("Refresh");
    }

    private void NotifyTryOpenRod()
    {
        _rod0.SendMessage("Click");
        _rod1.SendMessage("Click");
        _rod2.SendMessage("Click");
        _rod3.SendMessage("Click");
    }

    void OnRightButtonPress()
    {
        NotifyTryOpenRod();
    }

    void OnPositionFound()
    {
        //_blinkBg.SetActive(true);
        _lockAnim.enabled = true;
        Invoke("hide", 0.5f);
    }

    void hide()
    {
        _blinkBg.SetActive(false);
        _lockAnim.enabled = false;
    }
}

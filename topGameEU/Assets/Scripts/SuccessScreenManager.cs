﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SuccessScreenManager : MonoBehaviour {
    public Text _text;
    public AudioSource _cashSound;

    // Use this for initialization
    void Start () {
        _cashSound.Play();
        int money = PropertiesHandler.MoneyCosts[PropertiesHandler.LevelIndex];
        if (PropertiesHandler.IsTutorialGame)
        {
            money = Random.Range(20, 150);
        }
        else
        {
            PropertiesHandler.LevelIndex++;
        }
        PropertiesHandler.MoneyAll += money;
        if (PropertiesHandler.MoneyAll >= 160000)
        {
            SceneManager.LoadScene("OculusScene");
        }

        _text.text = money.ToString();
    }
}

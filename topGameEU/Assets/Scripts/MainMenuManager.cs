﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuManager : MonoBehaviour {
    public Text _text;
    public GameObject _continueButton;
    // Use this for initialization
    void Start () {
        _continueButton.SetActive(false);
        if (PropertiesHandler.MoneyAll >= 160000)
        {
            SceneManager.LoadScene("OculusScene");
        }
        _text.text = PropertiesHandler.MoneyAll.ToString() + " / 160000";
        if (PropertiesHandler.MoneyAll != 0)
        {
            _continueButton.SetActive(true);
        }
    }
}

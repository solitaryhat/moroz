﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuButtonActions : MonoBehaviour {

	public void StartTutorial()
    {
        PropertiesHandler.IsTutorialGame = true;
        PropertiesHandler.LevelIndex = 0;
        PropertiesHandler.MoneyAll = 0;
        SceneManager.LoadScene("GameScene");
    }

    public void StartGame()
    {
        PropertiesHandler.MoneyAll = 0;
        PropertiesHandler.IsTutorialGame = false;
        PropertiesHandler.LevelIndex = 0;
        SceneManager.LoadScene("GameScene");
    }

    public void ToMain()
    {
        PropertiesHandler.IsTutorialGame = false;
        SceneManager.LoadScene("MainMenu");
    }

    public void NextCrime()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void ReturnFromJail()
    {
        PropertiesHandler.LevelIndex = 0;
        PropertiesHandler.MoneyAll = 0;
        SceneManager.LoadScene("MainMenu");
    }

    public void OnContinueTouched()
    {
        SceneManager.LoadScene("GameScene");
    }
}

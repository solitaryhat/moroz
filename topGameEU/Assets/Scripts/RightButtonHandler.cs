﻿using UnityEngine;
using UnityEngine.EventSystems;

public class RightButtonHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public GameObject _gameCycle;

    private static bool _isTouched = false;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!_isTouched)
        {
            _gameCycle.SendMessage("OnRightButtonPress");
        }
        _isTouched = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _isTouched = false;
    }

    public static bool IsTouched()
    {
        return _isTouched;
    }
}
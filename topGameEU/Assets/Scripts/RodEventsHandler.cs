﻿using UnityEngine;
using System.Collections;

public class RodEventsHandler : MonoBehaviour {

    public int _id;
    public GameObject _eventHandler;

    AudioSource _audioSource;
    float _openPosition;
    float _delta;
    Rigidbody2D _rigidBody;
    private const int Y = 62;

    void Start () {
        _audioSource = GetComponent<AudioSource>();
        _openPosition = Random.Range(5, 50);
        _delta = 10;
        _rigidBody = GetComponent<Rigidbody2D>();
    }

	void Update () {
        if (!_rigidBody.isKinematic)
        {
            if (gameObject.transform.position.y >= _openPosition - 1 &&
                gameObject.transform.position.y <= _openPosition + 1)
            {
                _audioSource.Play();
                _eventHandler.SendMessage("OnPositionFound");
            }
        }
    }

    private void TryHold()
    {
        if (gameObject.transform.position.y >= _openPosition - _delta &&
            gameObject.transform.position.y <= _openPosition + _delta)
        {
            Open();
        }
        else
        {
            Close();
        }
    }

    private void Open()
    {
        _rigidBody.isKinematic = true;
        _eventHandler.SendMessage("OnOpen", _id);
    }

    private void Close()
    {
        _eventHandler.SendMessage("OnBroke", _id);
    }

    void Refresh()
    {
        transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        _rigidBody.isKinematic = false;
    }

    void Click()
    {
        if (!_rigidBody.isKinematic && transform.position.y >= 5)
        {
            TryHold();
        }
    }
}
